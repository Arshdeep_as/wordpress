<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp1');

/** MySQL database username */
define('DB_USER', 'web_user');

/** MySQL database password */
define('DB_PASSWORD', 'mypass');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pX/&N/Z:}UDHLtlx/-]gsmGfHqS|/5%c6s*;FMaLL*f_t0z>1IG|L^AK].%)PW?d');
define('SECURE_AUTH_KEY',  ':SQ(AhS7;TgK:UL59Oe_#Gv3UL0yX?)GQjVVEe&MIg,;4@`@0><d>NJ=9p |1cTX');
define('LOGGED_IN_KEY',    'nD.It/un2J/j0T!`lC:ZAe+mctN@md*%T%DO4Q9Hvb@ku7(j1k`9Lu-4jC%v5;Bg');
define('NONCE_KEY',        'M~?&@KPSNd$#KT@puz{-pm(A2:SMI` ,n;K%j^2e@((vbTtG;UoPv%OSP-vq|gf]');
define('AUTH_SALT',        'k-|p x-{/@9LN{8TCO!y1~JFfW=Q3osC2+m4=B7ezz.E<n%uid;4{h @:@jZ8_1T');
define('SECURE_AUTH_SALT', '*55L1RniTSlTht>d?u*;L+-c1EZH&gJr2u1{O1NWpv~Iwy{o@WWQB=}k.NH}A.ut');
define('LOGGED_IN_SALT',   'AlcKUO5k|iiJ]~s)ezEiw/|>%9v*j;r|+H<ALrk-Y4^;^vV:TygG/IfO&rrBz~7n');
define('NONCE_SALT',       'c~2feCjeym58X}%Un6mZ{XMyraF}1(aGXlz3wnodsv!Gp8j21;6ZZFk%w--6|J[r');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'WP1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
