<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp0');

/** MySQL database username */
define('DB_USER', 'web_user');

/** MySQL database password */
define('DB_PASSWORD', 'mypass');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N#%=H$lp&Oa^]AQo>3F)A@7ihj~v5mBewgav+3Z=*_*VT.8{~rM[CR A|.>N>Z|k');
define('SECURE_AUTH_KEY',  '6?[R)g$?J;N>&1YTHumq__bnKL#i,e>,K!A=u8(pl!%1L?.tVI{_-QHPgYBugzfi');
define('LOGGED_IN_KEY',    '8].8sP7W;b+}9-2R[}C=9zq#AI*l/l=G-kJ{,z-Y1S~zfUho090Q[@~xNxR<sn]h');
define('NONCE_KEY',        ')yb7!_np$&p-n]*0Xn6`D-A-e<+)l|3|ZuJn7FrrpT$M/ew_E9FZ;Vt3DrvG|y$n');
define('AUTH_SALT',        '5iZ4N_~*1(eS0.45u3QVGP)8 FHFvRw_uwa8S.5<id>D>zH9RP%{zXxnd43-}x]l');
define('SECURE_AUTH_SALT', 'o+.Syh-^6`J}~${;p}S0tP(Gd6>q664%V4Jz.Vx0F(N|GZM,x<v~(V>8HvO(DpUl');
define('LOGGED_IN_SALT',   '$#nzbzClgeTY@_Z[U5z$i/> ?4HL~n?:wXGR}VT#r#&4O4[&o_Cn0UQ!rV3q^PPb');
define('NONCE_SALT',       '{(qbkY=LP`Y@pf1-uDrvO2bu8Pjs-SW*:cTbAz={#?6{~X^+3,nw&.HJ-rpbzTtl');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wdd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
