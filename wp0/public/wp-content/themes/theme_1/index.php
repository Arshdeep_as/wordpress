<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"></meter>
	<title>My Blog</title>

	<link rel="stylesheet" type="text/css" href="<?=get_stylesheet_uri()?>">
	<?php wp_head(); ?>
</head>
<body>
	<div id="container">
		<div id="main">
			<h1>My Blog</h1>

			<?php while(have_posts()) : ?>
				<?php the_post(); ?>
				<?php the_title('<h1>','</h1>'); ?>
				<p><small>Posted on <?php the_date();?></small></p>
				<a href="<?php the_permalink(); ?>">READ MORE....</a>
				<?php the_content('<div>','</div>'); ?>
			<?php endwhile; ?>
		</div> <!-- ends main -->

		<div id="sidebar">
			<h2>Sidebar</h2>
			<h3>Menu</h3>
			<?php wp_nav_menu(); ?>
			<h3>Search</h3>
			<form action="/" method="get">
				<input type="text" name="s" />
				<input type="submit" name="search" />
			</form>
			<h3>Archives</h3>
			<?php wp_get_archives() ?>

			<h3>Category</h3>
			<?php wp_list_categories('title_li') ?>
		</div> <!-- ends sidebar -->

	</div><!-- ends conainer -->
	<?php wp_footer(); ?>
</body>
</html>